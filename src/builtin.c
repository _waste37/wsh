#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <wsh.h>

static int32_t
exit_builtin(int32_t argc, const char **argv)
{
    switch (argc) {
        case 1: 
        {
            wsh_printinfo("exit\n");
            exit(0);
        }
        case 2: {
            wsh_printinfo("exit\n");
            long i = strtol(argv[1], NULL, 0);
            exit(i);
        }
        default:
            wsh_printerr("exit: too many arguments\n");
            return -1;
    }
}

static int32_t 
cd_builtin(int32_t argc, const char **argv)
{
    int32_t ret;
    const char *dir;
    switch (argc) {
        case 1: 
        {
            dir = getenv("HOME");
            if (dir == NULL) {
                wsh_printerr("cd: HOME not set\n");
                return -1;
            }
            break;
        }
        case 2: 
            dir = argv[1];
            break;
        default:
            wsh_printerr("cd: too many arguments\n");
            return -1;
    }
    if((ret = chdir(dir)) != 0) {
        wsh_printerr("cd: %s\n", strerror(errno));
    }
    return ret;
}

typedef struct {
    const str name;
    int32_t (*func)(int32_t, const char **);
} wsh_builtin;

static const wsh_builtin wsh_builtins[] = {
    { .name = {.len = 3, .raw = "cd" },
      .func = cd_builtin
    },
    { .name = {.len = 5, .raw = "exit"},
      .func = exit_builtin
    }
};

bool
wsh_builtin_parse(wsh_command *c) 
{
    for(size_t i = 0; i < sizeof(wsh_builtins) / sizeof (wsh_builtin); ++i) {
        if (str_equal(c->argv.ss, &wsh_builtins[i].name)) {
            c->type = WSH_COMMAND_BUILTIN;
            c->builtin_func = wsh_builtins[i].func;
            return true;
        }
    }
    return false;
}
