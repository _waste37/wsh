#include <string.h>
#include <sys/wait.h>

#include <wsh.h>

struct state {
    strvec path;
};

static struct state wsh_state = {0};

/*
 * abitlity to launch things from cli
 * support for colors
 * stuff like completion, syntax hightlight and shit
 * using path? probably yes
 */

extern char **environ;

bool 
wsh_read_line(str *s, FILE *stream)
{
    str_getline(s, stream);
    if (s->raw == NULL) {
        if (!feof(stream)) {
            wsh_log_error(strerror(errno));
            exit(EXIT_FAILURE);
        } else {
            return false;
        }
    }

    str_trim(s);
    return true;
}

void
wsh_com_init(wsh_command *c)
{
    c->detach = false;
    str_init(&c->bin_path);
    strvec_init(&c->argv);
}

void
wsh_com_free(wsh_command *c)
{
    if (c->bin_path.raw != NULL) {
        str_free(&c->bin_path);
    }
    strvec_free(&c->argv);
}

void 
wsh_print_prompt(void)
{
    printf("$ ");
    fflush(stdout);
}

/* SIGNALS *******************************/

void sigint_shell(int s) 
{
    putchar('\n');
    wsh_print_prompt();
}

void 
sigint_subprocess(int s) 
{ 
    exit(255); 
}

/*****************************************/


int32_t
wsh_run(wsh_command *com)
{
    if (com->type == WSH_COMMAND_BUILTIN) {
        return com->builtin_func(com->argv.len - 1, com->argv.raw);
    }
    
    int32_t retval;
    pid_t pid = fork();
    if (pid < 0) {
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        wait(&retval);
        return retval;
    } else {
        signal(SIGINT, sigint_subprocess);
        execve(com->bin_path.raw, com->argv.raw, environ);
        exit(EXIT_FAILURE);
    }
}

bool
wsh_find_in_path(str *bin, str *name)
{
    struct dirent *dirp;
    DIR *dir;
    for (size_t i = 0; i < wsh_state.path.len; ++i) {
        str *dir_path = &wsh_state.path.ss[i];
        if((dir = opendir(dir_path->raw)) == NULL) {
            wsh_log_error(strerror(errno));
        }

        while ((dirp = readdir(dir)) != NULL) {
            if(strcmp(dirp->d_name, name->raw) == 0) {
                str_append(bin, dir_path);
                str_insert(bin, '/', bin->len - 1);
                str_append(bin, name);
                return true;
            }
        }
    }
    return false;
}

bool
wsh_compile(wsh_command *c, str *input)
{
    str s;
    while(input->raw) {
        s = str_split(input, ' ');
        str_push(&s, '\0');
        strvec_push(&c->argv, &s);
    }
    strvec_push_cstr(&c->argv, NULL);

    if(wsh_builtin_parse(c)) {
        return true;
    }

    if (!wsh_find_in_path(&c->bin_path, &c->argv.ss[0])) {
        return false;
    }

    c->type = WSH_COMMAND_BINARY;
    return true;
}

void 
wsh_interpret(str *input)
{
    wsh_command com;
    wsh_com_init(&com);
    if(!wsh_compile(&com, input)) {
        wsh_printerr("command '%s' not found in path\n", com.argv.raw[0]);
        wsh_com_free(&com);
        return;
    }

    int32_t ret = wsh_run(&com);
    if (ret != 0) {
        wsh_printinfo("command returned %d\n", ret);
    }
    wsh_com_free(&com);
}

void 
wsh_init(void)
{
    signal(SIGINT, sigint_shell);

    strvec_init(&wsh_state.path);
    const char *path_var = getenv("PATH");
    if (path_var == NULL) {
        wsh_log_error("internal path not implemented");
        exit(EXIT_FAILURE);
    }
    str tmp;
    str_init_from(&tmp, path_var);

    str s;
    while(tmp.raw) {
        s = str_split(&tmp, ':');
        str_push(&s, '\0');
        strvec_push(&wsh_state.path, &s);
    }
    wsh_log_info("started succesfully");
}

int 
main(int argc, const char **argv)
{
    wsh_init();
//    wsh_input_init();
    wsh_print_prompt();
    str input;
    while (wsh_read_line(&input, stdin)) {
        if (input.len) {
            wsh_interpret(&input);
        }
        wsh_print_prompt();
    }

    puts("\nexit");
    return 0;
}
