#include <stdarg.h>
#include <stdio.h>
#include <wsh.h>

#define RED      "\x1b[31m"
#define GREEN    "\x1b[32m"
#define YELLOW   "\x1b[0;33m"
#define BRYELLOW "\x1b[1;33m"
#define BLUE     "\x1b[34m"
#define MAGENTA  "\x1b[35m"
#define CYAN     "\x1b[36m"
#define RESET    "\x1b[0m"


void 
wsh_printerr(const char *fmt, ...)
{
    fprintf(stderr, RED"error: "RESET);

    va_list ap;
    va_start(ap, NULL);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
}

void 
wsh_printwrn(const char *restrict fmt, ...)
{
    fprintf(stderr, RED"warning: "RESET);
    
    va_list ap;
    va_start(ap, NULL);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
}

void 
wsh_printinfo(const char *fmt, ...)
{
    fprintf(stderr, GREEN"info: "RESET);

    va_list ap;
    va_start(ap, NULL);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
}

void 
wsh_log_error__(const char *msg, const char *file, int line, const char *func)
{
    fprintf(stderr, RED"error: "CYAN"%s"RESET":"YELLOW"%d"RESET":"BRYELLOW"%s"RESET": %s\n", file, line, func, msg);
}

void 
wsh_log_warning__(const char *msg, const char *file, int line, const char *func)
{
    fprintf(stderr, YELLOW"warning: "CYAN"%s"RESET":"YELLOW"%d"RESET":"BRYELLOW"%s"RESET": %s\n", file, line, func, msg);
}

void 
wsh_log_info__(const char *msg, const char *file, int line, const char *func)
{
    fprintf(stderr, GREEN"info: "CYAN"%s"RESET":"YELLOW"%d"RESET":"BRYELLOW"%s"RESET": %s\n", file, line, func, msg);
}

#ifndef NDEBUG
void 
wsh_log_debug__(const char *msg, const char *file, int line, const char *func)
{
    fprintf(stderr, MAGENTA"debug: "CYAN"%s"RESET":"YELLOW"%d"RESET":"BRYELLOW"%s"RESET": %s\n", file, line, func, msg);
}
#endif

