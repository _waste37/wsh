#ifndef WSH_H
#define WSH_H

#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
// black-flag headers
#include <str.h>

typedef enum {
    WSH_COMMAND_BINARY,
    WSH_COMMAND_BUILTIN
} wsh_command_type;

typedef struct {
    wsh_command_type type;
    bool detach;
    strvec argv;
    str bin_path;
    int32_t (*builtin_func)(int32_t, const char **);
} wsh_command;

bool wsh_builtin_parse(wsh_command *c);
//bool wsh_read_line(str *s);
//void wsh_input_init(void);


/* log utilities **************************************************************/

void wsh_printerr(const char *restrict fmt, ...);
void wsh_printwarn(const char *restrict fmt, ...);
void wsh_printinfo(const char *restrict fmt, ...);

void wsh_log_error__(const char *msg, const char *file, int line, const char *func);
#define wsh_log_error(x) wsh_log_error__(x, __FILE__, __LINE__, __func__)

void wsh_log_warning__(const char *msg, const char *file, int line, const char *func);
#define wsh_log_warning(x) wsh_log_warning__(x, __FILE__, __LINE__, __func__)

# ifdef WSH_RELEASE
# define wsh_log_info(x) ((void)0)
# else
void wsh_log_info__(const char *msg, const char *file, int line, const char *func);
# define wsh_log_info(x) wsh_log_info__(x, __FILE__, __LINE__, __func__)
# endif

# ifdef NDEBUG
# define wsh_log_debug(x) ((void)(0))
# else
void wsh_log_debug__(const char *msg, const char *file, int line, const char *func);
# define wsh_log_debug(x) wsh_log_debug__(x, __FILE__, __LINE__, __func__)
# endif

/******************************************************************************/

#endif /* WSH_H */
